﻿using System;
using System.Drawing;
using System.Timers;
using System.Collections;

namespace TetrisProject
{
    public class GameBoard
    {
        Timer timer;
      
        public bool Paused { get;private set; }
        public Cell[,] BoardMatrix { get; private set; }
        public int RowsNumber { get; set; }
        public int ColumnsNumber { get; set; }
        public int Score { get; set; }
        public int Level { get; set; }
        int _timerInterval;
        bool _started = false;
        public Figure NextFigure { get { return _nextFigure; }  }
        Figure _currentFigure, _nextFigure;

        public EventHandler ScoreChanged;
        public EventHandler BoardUpdated;
        public EventHandler NeedDrawNextFigure;
        public EventHandler GameOver;


        public GameBoard(int rowsNumber, int columnsNumber)
        {
            RowsNumber = rowsNumber;
            ColumnsNumber = columnsNumber;
            BoardMatrix = new Cell[rowsNumber, columnsNumber];
            for (int i = 0; i < RowsNumber; i++)
            {
                for (int j = 0; j < ColumnsNumber; j++)
                  BoardMatrix[i, j] = new Cell() { Status = CellStatus.Empty };                
            }
            if (BoardUpdated != null)
                BoardUpdated.Invoke(this, new EventArgs());
            Score = 0;

            _timerInterval = 500;
            timer = new Timer(_timerInterval);
            timer.Elapsed += Timer_Elapsed;
            //Paused = true;

        }
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Move(MoveType.Down);
        }

       

        public void Move(MoveType moveType)
        {
            if (Paused || !_started)
                return;
            
            Point newLocation = new Point();
            switch (moveType)
            {
                case MoveType.Down:
                    
                    
                    newLocation = new Point(_currentFigure.Location.X + 1, _currentFigure.Location.Y);

                    if (!CanPutFigureOnLocation(_currentFigure, newLocation) || _currentFigure.Location.X + _currentFigure.Height == RowsNumber)
                    {
                        foreach (var p in _currentFigure.GetAbsolutePoints())                        
                            BoardMatrix[p.X, p.Y].Status = CellStatus.Fixed;
                        
                        CheckForScore();
                        return;
                    }                        
                   
                    break;

                case MoveType.Right:
                    if (_currentFigure.Location.Y + _currentFigure.Width == ColumnsNumber)                    
                        return ;
                    

                    newLocation = new Point(_currentFigure.Location.X, _currentFigure.Location.Y + 1);

                    if (!CanPutFigureOnLocation(_currentFigure, newLocation))
                        return ;
                    
                    break;

                case MoveType.Left:
                    if (_currentFigure.Location.Y == 0)                   
                        return ;
                   
                    newLocation = new Point(_currentFigure.Location.X, _currentFigure.Location.Y - 1);

                    if (!CanPutFigureOnLocation(_currentFigure, newLocation))
                        return ;                 
                    break;               
            }

            //clear figure
            foreach (var p in _currentFigure.GetAbsolutePoints())           
                BoardMatrix[p.X, p.Y] = new Cell() { Status = CellStatus.Empty };

            _currentFigure.Location = newLocation;
            foreach (var p in _currentFigure.GetAbsolutePoints())            
                BoardMatrix[p.X, p.Y] = new Cell() { Status = CellStatus.Moving, CellColor = _currentFigure.BlockColor };           

            if (BoardUpdated != null)            
                BoardUpdated.Invoke(this, new EventArgs());         

        }
        public void Rotate()
        {
            if (Paused || !_started)
                return;

            RotationAngle newAngle = RotationAngle.Deg0;
            if (_currentFigure.Angle != RotationAngle.Deg270)
            {
                int angleIndex = (int)_currentFigure.Angle;
                angleIndex++;
                newAngle = (RotationAngle)angleIndex;
            }


            Figure newFigure = _currentFigure.GetRotatation(newAngle);
            foreach (var p in newFigure.GetAbsolutePoints())
            {
                try
                {
                    if (BoardMatrix[p.X, p.Y].Status == CellStatus.Fixed)
                        return;
                }
                catch//out of bound
                {
                    return;
                }
            }

            //clear figure
            foreach (var p in _currentFigure.GetAbsolutePoints())
                BoardMatrix[p.X, p.Y] = new Cell() { Status = CellStatus.Empty };

            _currentFigure = newFigure;

            foreach (var p in _currentFigure.GetAbsolutePoints())
                BoardMatrix[p.X, p.Y] = new Cell() { Status = CellStatus.Moving, CellColor = _currentFigure.BlockColor };

            if (BoardUpdated != null)
                BoardUpdated.Invoke(this, new EventArgs());
        }
        void CheckForScore()
        {
            list
            for (int row = _currentFigure.Location.X; row < _currentFigure.Location.X + _currentFigure.Height; row++)
            {
                bool rowComplete = true;
                for (int cell = 0; cell < ColumnsNumber; cell++)
                {
                    if (BoardMatrix[row, cell].Status == CellStatus.Empty)
                    {
                        rowComplete = false;
                        break;
                    }
                }
                if (rowComplete)
                {
                    for (int c = 0; c < ColumnsNumber; c++)
                    {
                        BoardMatrix[row, c].Status = CellStatus.Score;
                    }
                    if (ScoreChanged != null)
                        ScoreChanged.Invoke(this, new EventArgs());
                    if (BoardUpdated != null)
                        BoardUpdated.Invoke(this, new EventArgs());

                    Pause();
                    System.Threading.Thread.Sleep(500);
                    Pause(); 

                    for (int i = row; i >= 1; i--)
                    {
                        for (int j = 0; j < ColumnsNumber; j++)
                        {
                            BoardMatrix[i, j] = BoardMatrix[i - 1, j];
                        }
                    }

                    for (int j = 0; j < ColumnsNumber; j++)
                    {
                        BoardMatrix[0, j] = new Cell() { Status = CellStatus.Empty };
                    }
                    Score++;
                    if (ScoreChanged != null)
                        ScoreChanged.Invoke(this, new EventArgs());
                    if (BoardUpdated != null)
                        BoardUpdated.Invoke(this, new EventArgs());

                }
            }
            CreateNewFigure();
        }
     

        public void Start()
        {
            if (_started)
                return;

            _started = true;
            CreateNewFigure();
            timer.Enabled = true;
            Paused = false;
          
        }
        /// <summary>
        /// Pause game or continue paused game
        /// </summary>
        public void Pause()
        {
            timer.Enabled = Paused;
            Paused = !Paused;            
        }
        void CreateNewFigure()
        {
            if (_nextFigure==null)
            {
                _nextFigure = FigureFactory.GetRandomFigure();
            }
            _currentFigure = _nextFigure;
            _currentFigure.Location = new Point(0, (this.ColumnsNumber - _currentFigure.Width) / 2);
            
            if (!CanPutFigureOnLocation(_currentFigure, _currentFigure.Location))
            {    
                //GameOver
                if (GameOver != null)
                {   GameOver.Invoke(this, new EventArgs());
                    return;
                }
            }
            _nextFigure = FigureFactory.GetRandomFigure();
            if (NeedDrawNextFigure != null)
                NeedDrawNextFigure.Invoke(this, new EventArgs());


            foreach (var p in _currentFigure.GetAbsolutePoints())          
                BoardMatrix[p.X, p.Y] = new Cell() { Status = CellStatus.Moving, CellColor = _currentFigure.BlockColor };
           

            if (BoardUpdated != null)            
                BoardUpdated.Invoke(this, new EventArgs());
           

        }
       

        bool CanPutFigureOnLocation(Figure figure, Point location)
        {
            foreach (var p in figure.RelativePoints)
            {
                try
                {
                    if (BoardMatrix[p.X + location.X, p.Y + location.Y].Status == CellStatus.Fixed)
                    {
                        return false;
                    }
                }
                catch//out of bound
                {
                    return false;
                }
            }
            return true;
        }


    }
    public enum CellStatus { Empty, Moving, Fixed, Score }
    public class Cell
    {
        public CellStatus Status { get; set; }
        public Color CellColor { get; set; }
    }
}
