﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TetrisProject
{
   public class FigureFactory
    {
        static Random _random;
        static FigureFactory()
        {
            _random = new Random(DateTime.Now.Millisecond);
        }
        public static Figure GetRandomFigure()
        {
           
            Array values = Enum.GetValues(typeof(RotationAngle));
            RotationAngle randomAngle = (RotationAngle)values.GetValue(_random.Next(values.Length));
            //return new Corner(RotationAngle.Deg0);
            int i = _random.Next(1, 12);
            switch (i)
            {
                case 1:
                    return new Line(randomAngle);
                case 2:
                    return new Square(randomAngle);
                case 3:
                    return new HalfLine(randomAngle);
                case 4:
                    return new Dot(randomAngle);
                case 5:
                    return new RightT(randomAngle);
                case 6:
                    return new LeftT(randomAngle);
                case 7:
                    return new FullT(randomAngle);
                case 8:
                    return new RThunder(randomAngle);
                case 9:
                    return new LThunder(randomAngle);
                case 10:
                    return new Corner(randomAngle);
                case 11:
                    return new Plus(randomAngle);

                default:
                    return new Line(randomAngle);
            }
        }                        
      
    }
}
