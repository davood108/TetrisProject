﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Collections;

namespace TetrisProject
{
    public abstract class Figure
    {
       
        public Point Location { get; set; }
        public int Height { get;protected set; }
        public int Width { get; protected set; }

        public Color BlockColor { get; set; }
        public RotationAngle Angle { get; protected set; }

        /// <summary>
        /// Relative coordinates of points of the figure
        /// </summary>
        public abstract List<Point> RelativePoints { get; set; }
        
        public Figure(RotationAngle angle)
        {            
            InitializePoints(angle);
            Angle = angle;

        }
        /// <summary>
        /// Get absolute coordinates of points of the figure on board
        /// </summary>
        public List<Point> GetAbsolutePoints()
        {
            List<Point> indexes = new List<Point>();
            foreach (var p in RelativePoints)
            {
                Point point = new Point(p.X + Location.X, p.Y + Location.Y);
                indexes.Add(point);
            }
            return indexes;
        }
        public abstract void InitializePoints(RotationAngle angle);
        /// <summary>
        /// Rotate the figure and return new figure with same location
        /// </summary>      
        public abstract Figure GetRotatation(RotationAngle newAngle);
     
    }

    public enum RotationAngle { Deg0, Deg90, Deg180, Deg270 }
    public enum MoveType { Down, Right, Left }
}
