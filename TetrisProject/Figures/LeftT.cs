﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TetrisProject
{
    public class LeftT : Figure
    {
       
        public LeftT(RotationAngle angle) : base(angle)
        {
            BlockColor = Color.Brown;
        }
        public override void InitializePoints(RotationAngle angle)
        {
            RelativePoints = new List<Point>();
            switch (angle)
            {
                case RotationAngle.Deg0:
                    RelativePoints.Add(new Point(0, 0));
                    RelativePoints.Add(new Point(0, 1));
                    RelativePoints.Add(new Point(1, 1));
                    RelativePoints.Add(new Point(2, 1));
                    Width = 2;
                    Height = 3;
                    break;
                case RotationAngle.Deg90:
                    RelativePoints.Add(new Point(0, 2));
                    RelativePoints.Add(new Point(1, 0));
                    RelativePoints.Add(new Point(1, 1));
                    RelativePoints.Add(new Point(1, 2));
                    Width = 3;
                    Height = 2;

                    break;
                case RotationAngle.Deg180:

                    RelativePoints.Add(new Point(0, 0));
                    RelativePoints.Add(new Point(1, 0));                  
                    RelativePoints.Add(new Point(2, 0));
                    RelativePoints.Add(new Point(2, 1));
                    Width = 2;
                    Height = 3;
                    break;
                case RotationAngle.Deg270:
                    RelativePoints.Add(new Point(0, 0));
                    RelativePoints.Add(new Point(0, 1));
                    RelativePoints.Add(new Point(0, 2));
                    RelativePoints.Add(new Point(1, 0));
                    Width = 3;
                    Height = 2;

                    break;
                default:
                    break;
            }



        }

        public override Figure GetRotatation(RotationAngle newAngle)
        {
            return new LeftT(newAngle) { Location = this.Location };
        }

        public override List<Point> RelativePoints { get; set; }
    }
}
