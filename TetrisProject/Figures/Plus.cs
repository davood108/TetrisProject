﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TetrisProject
{
    public class Plus : Figure
    {

        public Plus(RotationAngle angle) : base(angle)
        {
            BlockColor = Color.Turquoise;
        }
        public override void InitializePoints(RotationAngle angle)
        {
            RelativePoints = new List<Point>();
            RelativePoints.Add(new Point(0, 1));
            RelativePoints.Add(new Point(1, 0));         
            RelativePoints.Add(new Point(1, 1));
            RelativePoints.Add(new Point(1, 2));
            RelativePoints.Add(new Point(2, 1));
            Height = 3;
            Width = 3;
            
        }

        public override Figure GetRotatation(RotationAngle newAngle)
        {
            return this;
        }

        public override List<Point> RelativePoints { get; set; }
    }
}
