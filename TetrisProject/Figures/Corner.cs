﻿using System.Collections.Generic;
using System.Drawing;

namespace TetrisProject
{
    public class Corner : Figure
    {

        public Corner(RotationAngle angle) : base(angle)
        {
            BlockColor = Color.DarkGoldenrod;
        }
        public override void InitializePoints(RotationAngle angle)
        {
            RelativePoints = new List<Point>();
            Width = 2;
            Height = 2;
            switch (angle)
            {
                case RotationAngle.Deg0:
                    RelativePoints.Add(new Point(0, 0));
                    RelativePoints.Add(new Point(0, 1));
                    RelativePoints.Add(new Point(1, 0));                                     
                    break;
                case RotationAngle.Deg90:
                    RelativePoints.Add(new Point(0, 0));
                    RelativePoints.Add(new Point(0, 1));                  
                    RelativePoints.Add(new Point(1, 1));
                    break;
                case RotationAngle.Deg180:                 
                    RelativePoints.Add(new Point(0, 1));
                    RelativePoints.Add(new Point(1, 0));
                    RelativePoints.Add(new Point(1, 1));
                    break;
                case RotationAngle.Deg270:
                    RelativePoints.Add(new Point(0, 0));
                    RelativePoints.Add(new Point(1, 0));                   
                    RelativePoints.Add(new Point(1, 1));
                    break;
                default:
                    break;
            }



        }

        public override Figure GetRotatation(RotationAngle newAngle)
        {

            return new Corner(newAngle) { Location = this.Location };
        }

        public override List<Point> RelativePoints { get; set; }
    }
}
