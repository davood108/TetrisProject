﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TetrisProject
{
    public class HalfLine : Figure
    {
       
        public HalfLine(RotationAngle angle) : base(angle)
        {
            BlockColor = Color.Blue;
        }


        public override void InitializePoints(RotationAngle angle)
        {
            RelativePoints = new List<Point>();
            if (angle == RotationAngle.Deg0 || angle == RotationAngle.Deg180)
            {
                RelativePoints.Add(new Point(0, 0));
                RelativePoints.Add(new Point(0, 1));
                Height = 1;
                Width = 2;
            }
            else
            {
                RelativePoints.Add(new Point(0, 0));
                RelativePoints.Add(new Point(1, 0));
                Height = 2;
                Width = 1;
            }

        }


        public override Figure GetRotatation(RotationAngle newAngle)
        {
           
            return new HalfLine(newAngle) { Location = this.Location };
        }


        public override List<Point> RelativePoints { get; set; }
    }
}
