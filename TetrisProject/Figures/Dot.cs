﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TetrisProject
{
   public class Dot:Figure
    {
       
        public Dot(RotationAngle angle) : base(angle)
        {
            BlockColor = Color.SkyBlue;
        }
        public override void InitializePoints(RotationAngle angle)
        {
            RelativePoints = new List<Point>();
            RelativePoints.Add(new Point(0, 0));           
            Height = 1;
            Width = 1;
        }

        public override Figure GetRotatation(RotationAngle newAngle)
        {
            return this;
        }

        public override List<Point> RelativePoints { get; set; }
    }
}
