﻿
using System.Collections.Generic;
using System.Drawing;

namespace TetrisProject
{
    public class FullT : Figure
    {

        public FullT(RotationAngle angle) : base(angle)
        {
            BlockColor = Color.Purple;
        }
        public override void InitializePoints(RotationAngle angle)
        {
            RelativePoints = new List<Point>();
            switch (angle)
            {
                case RotationAngle.Deg0:
                    RelativePoints.Add(new Point(0, 0));
                    RelativePoints.Add(new Point(0, 1));
                    RelativePoints.Add(new Point(0, 2));
                    RelativePoints.Add(new Point(1, 1));
                    Width = 3;
                    Height = 2;
                    break;
                case RotationAngle.Deg90:
                    RelativePoints.Add(new Point(1, 0));
                    RelativePoints.Add(new Point(0, 1));
                    RelativePoints.Add(new Point(1, 1));
                    RelativePoints.Add(new Point(2, 1));                    
                    Width = 2;
                    Height = 3;

                    break;
                case RotationAngle.Deg180:

                    RelativePoints.Add(new Point(0, 1));
                    RelativePoints.Add(new Point(1, 0));
                    RelativePoints.Add(new Point(1, 1));
                    RelativePoints.Add(new Point(1, 2));
                    Width = 3;
                    Height = 2;
                    break;
                case RotationAngle.Deg270:
                    RelativePoints.Add(new Point(0, 0));
                    RelativePoints.Add(new Point(1, 0));
                    RelativePoints.Add(new Point(2, 0));
                    RelativePoints.Add(new Point(1, 1));
                    Width = 2;
                    Height = 3;

                    break;
                default:
                    break;
            }



        }

        public override Figure GetRotatation(RotationAngle newAngle)
        {

            return new FullT(newAngle) { Location = this.Location };
        }

        public override List<Point> RelativePoints { get; set; }
    }
}

