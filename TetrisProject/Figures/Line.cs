﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TetrisProject
{
    public class Line:Figure
    {
      
        public Line(RotationAngle angle) : base(angle)
        {
            BlockColor = Color.Green;
        }


        public override void InitializePoints(RotationAngle angle)
        {
            RelativePoints = new List<Point>();
            if (angle == RotationAngle.Deg0 || angle == RotationAngle.Deg180)
            {
                RelativePoints.Add(new Point(0, 0));
                RelativePoints.Add(new Point(0, 1));
                RelativePoints.Add(new Point(0, 2));
                RelativePoints.Add(new Point(0, 3));
                Height = 1;
                Width = 4;
            }
            else
            {
                RelativePoints.Add(new Point(0, 0));
                RelativePoints.Add(new Point(1, 0));
                RelativePoints.Add(new Point(2, 0));
                RelativePoints.Add(new Point(3, 0));
                Height = 4;
                Width = 1;
            }

        }

        public override Figure GetRotatation(RotationAngle newAngle)
        {
            
            return new Line(newAngle) { Location = this.Location };
        }

        public override List<Point> RelativePoints { get; set; }
    }
}
