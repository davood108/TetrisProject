﻿
using System.Collections.Generic;
using System.Drawing;

namespace TetrisProject
{
    public class RThunder : Figure
    {

        public RThunder(RotationAngle angle) : base(angle)
        {
            BlockColor = Color.DarkBlue;
        }
        public override void InitializePoints(RotationAngle angle)
        {
            RelativePoints = new List<Point>();
            if (angle == RotationAngle.Deg0 || angle == RotationAngle.Deg180)
            {
                RelativePoints.Add(new Point(0, 1));
                RelativePoints.Add(new Point(1, 0));
                RelativePoints.Add(new Point(1, 1));
                RelativePoints.Add(new Point(2, 0));
                Width = 2;
                Height = 3;
                
            }
            else
            {
                RelativePoints.Add(new Point(0, 0));
                RelativePoints.Add(new Point(0, 1));
                RelativePoints.Add(new Point(1, 1));
                RelativePoints.Add(new Point(1, 2));
                Width = 3;
                Height = 2;
            }       
        }

        public override Figure GetRotatation(RotationAngle newAngle)
        {

            return new RThunder(newAngle) { Location = this.Location };
        }

        public override List<Point> RelativePoints { get; set; }
    }
}

