﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TetrisProject
{
    public class Square : Figure
    {
       
        public Square(RotationAngle angle) : base(angle)
        {
            BlockColor = Color.YellowGreen;
        }
        public override void InitializePoints(RotationAngle angle)
        {
            RelativePoints = new List<Point>();
            RelativePoints.Add(new Point(0, 0));
            RelativePoints.Add(new Point(0, 1));
            RelativePoints.Add(new Point(1, 0));
            RelativePoints.Add(new Point(1, 1));

            Height = 2;
            Width = 2;
        }

        public override Figure GetRotatation(RotationAngle newAngle)
        {
            return this;
        }

        public override List<Point> RelativePoints { get; set; }
    }
}
