﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace TetrisProject
{
    public partial class FrmTetris : Form
    {
       
        Figure nextFigure;
        GameBoard gameBoard;
        int cellWidth = 30;
        public FrmTetris()
        {
            InitializeComponent();
        }

        private void FrmTetris_Load(object sender, EventArgs e)
        {
            gameBoard = new GameBoard(20, 10);
            gameBoard.ScoreChanged += UpdateScore;
            gameBoard.NeedDrawNextFigure += DrawNextFigure;
            gameBoard.GameOver += GameOver;
            gameBoard.BoardUpdated += UpdateBoard;          
            gameBoard.Start();          
        }

        private void UpdateBoard(object sender, EventArgs e)
        {
            Graphics g = panel1.CreateGraphics();          
            for (int i = 0; i < gameBoard.RowsNumber; i++)
            {
                for (int j = 0; j < gameBoard.ColumnsNumber; j++)
                {
                    Cell cell = gameBoard.BoardMatrix[i, j];
                    Pen p = new Pen(Color.Black);
                    g.DrawRectangle(p, j * cellWidth, i * cellWidth, cellWidth, cellWidth);

                    if (cell.Status == CellStatus.Empty)
                    {                                                
                        Brush b = new SolidBrush(panel1.BackColor);                        
                        g.FillRectangle(b, j * cellWidth + 1, i * cellWidth + 1, cellWidth - 2, cellWidth - 2);
                    }
                    else if (cell.Status == CellStatus.Fixed || cell.Status == CellStatus.Moving)
                    {                       
                        Brush b = new SolidBrush(cell.CellColor);
                        Brush b2 = new LinearGradientBrush(new Point(cellWidth / 2, 0), new Point(cellWidth/2, cellWidth), Color.Black, cell.CellColor);
                        g.FillRectangle(b2, j * cellWidth + 1, i * cellWidth + 1, cellWidth - 2, cellWidth - 2);
                    }
                    else if (cell.Status == CellStatus.Score)
                    {
                        Brush b = new SolidBrush(cell.CellColor);
                        Brush b2 = new LinearGradientBrush(new Point(cellWidth / 2, 0), new Point(cellWidth / 2, cellWidth), Color.White, cell.CellColor);
                        g.FillRectangle(b2, j * cellWidth + 1, i * cellWidth + 1, cellWidth - 2, cellWidth - 2);
                    }
                }
            }

        }

        private void GameOver(object sender, EventArgs e)
        {
            Graphics g = panel1.CreateGraphics();
            Font f = new Font(new FontFamily("Tahoma"), 30);

            g.DrawString("Game Over", f, new SolidBrush(Color.Black), panel1.Width / 2 - 100, panel1.Height / 2);
        }

        private void DrawNextFigure(object sender, EventArgs e)
        {
           
            Graphics g = panel2.CreateGraphics();
            Brush brush = new SolidBrush(panel2.BackColor);
            g.FillRectangle(brush, 0, 0, 4 * cellWidth, 4 * cellWidth);
         
            nextFigure = gameBoard.NextFigure;
            foreach (var point in nextFigure.GetAbsolutePoints())
            {
                Pen p = new Pen(Color.Black);
                g.DrawRectangle(p, point.Y * cellWidth, point.X * cellWidth, cellWidth, cellWidth);
                Brush b = new SolidBrush(nextFigure.BlockColor);
                Brush b2 = new LinearGradientBrush(new Point(cellWidth / 2, 0), new Point(cellWidth / 2, cellWidth), Color.Black, nextFigure.BlockColor);
                g.FillRectangle(b2, point.Y * cellWidth + 1, point.X * cellWidth + 1, cellWidth - 2, cellWidth - 2);
            }
            
        }

        private void UpdateScore(object sender, EventArgs e)
        {
            lblScore.Invoke((MethodInvoker)(() => lblScore.Text = gameBoard.Score.ToString()));
        }


        private void FrmTetris_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right)
            {
                gameBoard.Move(MoveType.Right);
            }
            if (e.KeyCode == Keys.Left)
            {
                gameBoard.Move(MoveType.Left);
            }

            if (e.KeyCode == Keys.Down)
            {             
                gameBoard.Move(MoveType.Down);
            }
            if (e.KeyCode == Keys.Up)
            {
                gameBoard.Rotate();
            }
            if (e.KeyCode == Keys.Space)
            {
                gameBoard.Pause();
                lblPause.Visible = gameBoard.Paused;
            }

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            UpdateBoard(gameBoard, new EventArgs());
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            DrawNextFigure(gameBoard, new EventArgs());
        }
    }
}
